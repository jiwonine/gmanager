# G Manager (Geth Manager)

이더리움 데몬인 Geth 에서 발생하는 블록이벤트 기준으로, ERC20 토큰별 입금 이벤트를 받아오고, 관리하기 위한 프로그램이다. 
제공해주는 WEB3 API (ver.0.20.7) 기준으로 기능을 구성한다.



## 1. Introduction

기존의 ERC20 계열의 입금이벤트를 처리하는 ERC20-node 의 문제점을 개선하기 위해 개발하게 되었다.

**ERC20-node 의 문제점**

- Geth 데몬이 다운되거나 시작되었을 때, ERC20-node 프로그램도 재기동해야한다.
- Geth 데몬 다운된 후, ERC20-node 프로그램 재구동할 때까지, 전달받은 새로운 블록들은 누락되어 입금이벤트 체크를 못한다
- 이로 인해, ERC20 Token 입금을 놓쳐 수동으로 어드민 웹페이지에서 처리해야하는 번거로움이 발생한다

**G Manager 의 취지**

- Geth 데몬이 다운되고, 재구동되는 것과 별개로, G Manager 프로그램은 항상 기능을 수행할 수 있는 상태여야한다.
- Geth 데몬 다운된 후, 혹은 블록싱크를 제대로 못받았을 경우, 조회되는 마지막 블록을 항상 알고 있어야 한다.
- 다시 싱크가 정상적일 경우, G Manager 에서 가지고 있는 블록부터 Geth 데몬이 새로 싱크받는 마지막 블록까지의 이벤트를 체크한다.
- 이로 인해, 누락되는 부분 없이 블록이벤트를 처리하게 된다.



## 2. G Manager 구성도

```sequence
# Example of a comment
Note left of G Manager: **2-(1)**\n10.22.22.64
G Manager--> Was Geth Daemon: RPC Connection
Note left of Was Geth Daemon: **2-(2)**\n5초 주기로 Connction Check
Note over Was Geth Daemon, Web Geth Daemon: Block Sync\n Imported BlockInfo 
Note over Node1: BlockInfo
Node1->Web Geth Daemon: Import BlockSegment
Note over Node2: BlockInfo
Node2->Web Geth Daemon: Import BlockSegment
Web Geth Daemon--> Was Geth Daemon: Import BlockSegment!
Note over G Manager: **2-(3)**\nERC20 각 토큰별\n입금 이벤트 발생!!\n 3분 주기
G Manager-> SVC Wallet: /tokenName/wallet/deposit/add 'bodyMsg' SEND
Note right of SVC Wallet: 211.251.239.166
```



2-(1). G Manager 의 위치는 Scheduler 03 서버에서 구동된다. (기존의 ERC20-Node 프로그램)

2-(2). G Manager 는 RPC Connection 체크를 5초마다 진행하게 된다. 

만약에 Was Geth Daemon 이 내려갔다면, 주기적으로 Connection Check 를 한다.
다시 Geth Daemon 이 붙었다면, Geth 에서 제공해주는 Api (WEB3) 호출하여 사용할 수 있도록 한다.

2-(3). G Manager 에 설정되어있는 입금이벤트 처리주기(Timer:현재 3분) 에 맞춰서 ERC20 Contract 별로 확인한다.

- WAS Geth Daemon 에서 Block Number 를 받아 현재 블록정보로 저장
- 3분 후 다시 처리 시, WAS Geth Daemon 에서 현재 블록정보와 다르면 (블록사이즈 1이상 차이날 경우) 각 토큰별로 발생한 트랜잭션 조회

2-(4). ERC20, 누락된 고객입금 등록 API

- INPUT: 블록넘버, 토큰심볼명

- OUTPUT: 해당 블록넘버로, 누락된 모든 이벤트 체크 후 처리

- => 추후 API 규격은 정리할 예정


## 3. 현재까지 진행된 내역

위의 기준으로 개발완료 후, WAS 에 설치되어 있음. (9/8 이후로 G Manager 통해 ERC20 입금 처리중, erc20-node 프로그램 사용안함)

지금까지 프로그램이 다운되지 않았고, 입금 누락이벤트가 간간이 있어 확인해 본 결과 아래와 같이 문제점이 있음.



근 3주 입금누락 로그로 확인해보니 

**현재 ERC20 입금누락되는 경우는,  **

- 근 3주 지켜본 결과, 1주일에 간헐적으로 2-3번씩 블록정보를 읽었으나, 해당 입금 건을 못 읽을 경우가 있다.

  이 부분이 WEB3 Filter 에러인지, Syncing =true 인 경우, 블록의 FullNode 값이 없어서인지?는 확인이 필요하다. 

  이 부분은 두 가지 방안으로 확인이 필요하다.

  Syncing =true 인 경우, 현재 블록과 업데이트 해야 할 블록 차이로 인해 Event 를 제대로 못 읽어오는 것인지?

- 현재 ERC20 'Transfer' 만 입금처리됨. (9/12 오늘 날짜로 해당 메소드를 이용한 이벤트가 발생)

  =>  아래 링크와 같이 'ERC20 MethodID: sendMultiSigToken 추가' 필요

https://etherscan.io/tx/0x31e9ce367151dad5ecc614c44190ba8f79cc0bc096f33714e9d2d1b3719c2525



## 4. To do List

### 4-(1). ERC20 각, G Manager 별도 구성 필요

현재 ERC20 Token 추가은 총 13개 이고, 10월 이후에도 계속 추가될 예정이다. G Manager 에서 블록받아올 때 모든 정보를 다 서칭하기에는 부하가 걸릴 수 있기 때문에 2대의 장비에서 G Manager 구성할 필요가 있다.

- 이로 인해, Config "DpEventMode" = 'ERC20' MAX(ex. 15) 끊어서 따로 장비 추가필요함
- 해당 설정으로, 다른 로직으로 작동할 수 있어야 한다.



### 4-(2). Geth Daemon Syncing 일 경우, 

Syncing 상태인 경우, 현재 블록과 업데이트해야할 블록 차이로 인해 Event 를 제대로 못 읽어올 때가 발생한다.

근 3주 지켜본 결과, 1주일에 간헐적으로 2-3번씩 블록정보를 읽었으나, 해당 입금 건을 못읽을 경우가 있다.

이 부분이 WEB3 Filter 에러인지, Syncing 상태일 경우, 블록의 FullNode 값이 없어서인지는 확인이 필요하다. 

이 부분은 두 가지 방안으로 확인이 필요하다.

- **4-(2)-1. Syncing 인 경우, DepositEvent 처리 홀딩 후, 상태가 풀리면 재 동작 시 **

  **잘 읽어오는지? 통계적으로 얼마나 누락되는지? 데이터 산출해볼 필요가 있음 **

- **4-(2)-2. Web3 Filter 에러인 경우, .... WEB3 버전 업되기 전까진, 해당 사항 인지 후 버전 업 필요**

- **4-(2)-3. 혹, 누락된 경우에는 입금처리가능하도록 (해당블록넘버, 고객주소) API 제공 OK **



### 4-(3). 입금누락 처리 API 제공 (OK 완료, 2018-10-04)

- ERC20, 누락된 고객입금 등록 API
  - INPUT: 블록넘버, 토큰심볼명
  - OUTPUT: 해당 블록넘버로, 누락된 모든 이벤트 체크 후 처리

**Curl 사용 시 **

```
curl -X POST -H "Content-Type:application/json" -d "{\"blocknum\": \"6432393\",\"symbol\": \"CNUS\"}" http://127.0.0.1:5082/getDepositToken
```

**URL** http://xx.xx.xx.xx:5082/getDepositToken

**Input** - blocknum, symbol

**Output** - Success(result=1, count=5),  Fail(result=0, err='err message')

### **INPUT 데이터 설명**

| input key | input value | input description                                            |
| --------- | ----------- | ------------------------------------------------------------ |
| blocknum  | 6432393     | 해당 블록넘버 기입                                           |
| symbol    | "CNUS"      | GNT,GTO,NPXS,OMG,SNT,OBSR,PAX,AMO,ABLX,ABLD,TCO,TCOR,CNUS 중 선택입력<br />**(대문자)** |

### **OUTPUT 데이터 설명**

| 결과                   | output "key":"value"             | outputdescription                                            |
| ---------------------- | -------------------------------- | ------------------------------------------------------------ |
| 해당 조회 실패한 경우, | "result":"0", "err:"err Message" | 실패인 경우, 0                                               |
| 해당 조회 성공한 경우, | "result":"1", "count":"6"        | 성공한 경우, 1<br/>해당블록으로 성공건 수 전달<br/>해당블록으로 성공건 수가 0이여도 result=1 |

### **OUTPUT ERR Message**



### 4-(4). ERC20  sendMultiSigToken 이벤트 처리 추가

https://etherscan.io/tx/0x31e9ce367151dad5ecc614c44190ba8f79cc0bc096f33714e9d2d1b3719c2525






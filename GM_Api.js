
/** getDepositEth, MyAccount Find
input  -(M) blocknum [105067] 
        -(O) addr
**/
function getDepositEth (req, res) {
    const nblockNum = req.body.blocknum,
        nAddr = req.body.addr;

    var GManager  = require('./index.js');
    var cfg     = GManager.gCfg;
    var fsLog   = GManager.gLog;

    fsLog.apiLog.debug('------------------Request Data--/getDepositEth-');
    fsLog.apiLog.debug('BlockNum: '  + nAddr);
    // Optional
    if(nAddr) { fsLog.apiLog.debug('Addr: '+  nAddr);   }
    fsLog.apiLog.debug('---------------------------------------------------');

    var cfgFunc = cfg.cfgRoad();
    if(!cfgFunc) {
        console.log('GManager_config.json read Fail');
        fsLog.apiLog.info('------------------Response Data--/getDepositEth-');
        fsLog.apiLog.info(' result:0, err: GM_CONFIG_READ_FAIL');
        fsLog.apiLog.info('---------------------------------------------------');
        res.json({result:'0', err: 'GM_CONFIG_READ_FAIL'});
        return 'fail';
    }

    var Web3 = require("web3");
    var web3 = new Web3(new Web3.providers.HttpProvider(cfg.cfgRpcConn));

    if(web3.isConnected()!= true) {
        console.log('web3 connect fail ['+ cfg.cfgRpcConn +']');
        fsLog.apiLog.info('------------------Response Data--/getDepositEth-');
        fsLog.apiLog.info(' result:0, err: WEB3_CONN_FAIL');
        fsLog.apiLog.info('---------------------------------------------------');
        res.json({result:'0', err:'WEB3_CONN_FAIL'});
        return 'fail';
    }

    return 'success';
}

/** getDepositEth, MyAccount Find
input  -(M) blocknum [105067] 
        -(O) addr
**/
function sendTokenMsg(nSymbol, msgBody, index, web3) {
    var fetch   = require('node-fetch');
    var GManager  = require('./index.js');
    var cfg     = GManager.gCfg;
    var fsLog   = GManager.gLog;

    var depositUrl = cfg.cfgSvcWallet.concat(nSymbol, '/wallet/deposit/add');
    console.log('depositUrl:'+ depositUrl);

    fetch(depositUrl, {
        method: 'POST', 
        headers: {'Content-Type': 'application/json'},
        body: msgBody
    }).then(function(res) {
        fsLog.apiLog.info('--------------------------------------------------------------------------------------');
        fsLog.apiLog.info(' Erc20['+ nSymbol + '].['+ index +'] DepositUrl Response Code('+ res.status+')');
        fsLog.apiLog.info('--------------------------------------------------------------------------------------');
        if (res.status != 200) {
            fsLog.apiLog.info('res.statusErrCase');
            fsLog.apiLog.info('res.statusMsg='+ res.statusText);
            fsLog.apiLog.info('req.MsgBody=' + msgBody);
            fsLog.apiLog.info('--------------------------------------------------------------------------------------');
        }
    }).catch(err => console.log(err));

  
    return 'succ';
}

function getDepositToken (req, res) {
    const nblockNum = req.body.blocknum,
        nAddr = req.body.addr,
        nSymbol = req.body.symbol; 

    var GManager  = require('./index.js');
    var cfg     = GManager.gCfg;
    var fsLog   = GManager.gLog;

    fsLog.apiLog.debug('------------------Request Data--/getDepositToken-');
    fsLog.apiLog.debug('BlockNum: '  + nAddr);
    // Optional
    if(nAddr) { fsLog.apiLog.debug('Addr: '+  nAddr);   }
    fsLog.apiLog.debug('---------------------------------------------------');

    var cfgFunc = cfg.cfgRoad();
    if(!cfgFunc) {
        console.log('GManager_config.json read Fail');
        fsLog.apiLog.info('------------------Response Data--/getDepositToken-');
        fsLog.apiLog.info(' result:0, err: GM_CONFIG_READ_FAIL');
        fsLog.apiLog.info('---------------------------------------------------');
        res.json({result:'0', err: 'GM_CONFIG_READ_FAIL'});
        return 'fail';
    }

    var Web3 = require("web3");
    var web3 = new Web3(new Web3.providers.HttpProvider(cfg.cfgRpcConn));

    if(web3.isConnected()!= true) {
        console.log('web3 connect fail ['+ cfg.cfgRpcConn +']');
        fsLog.apiLog.info('------------------Response Data--/getDepositToken-');
        fsLog.apiLog.info(' result:0, err: WEB3_CONN_FAIL');
        fsLog.apiLog.info('---------------------------------------------------');
        res.json({result:'0', err:'WEB3_CONN_FAIL'});
        return 'fail';
    }

    var nContractAddress = 0, nMyWallet = 0;
    const listAcc = web3.personal.listAccounts;
    switch (nSymbol) {
        case "GNT"  : nContractAddress = "0xa74476443119A942dE498590Fe1f2454d7D4aC0d"; break;
        case "GTO"  : nContractAddress = "0xc5bbae50781be1669306b9e001eff57a2957b09d"; break;
        case "NPXS" : nContractAddress = "0xa15c7ebe1f07caf6bff097d8a589fb8ac49ae5b3"; break;
        case "OMG"  : nContractAddress = "0xd26114cd6EE289AccF82350c8d8487fedB8A0C07"; break;
        case "SNT"  : nContractAddress = "0x744d70fdbe2ba4cf95131626614a1763df805b9e"; break;
        case "TEST" : nContractAddress = "0xE249Be1b31ecD0C4fcef46d6d07516E8dFEf7bA2"; break;
        case "OBSR" : nContractAddress = "0xcb8a874ad773b10e6cf3b6b6de9dcc3b2ff68a9e"; break;
        case "AMO"  : nContractAddress = "0x38c87AA89B2B8cD9B95b736e1Fa7b612EA972169"; break;
        case "ABLX" : nContractAddress = "0x865bfd8232778f00cae81315bf75ef1fe6e30cdd"; break;
        case "ABLD" : nContractAddress = "0x5cfaf9ad2cb84ce2bc40be1beb735bd12a075e99"; break;
        case "TCOR" : nContractAddress = "0x4ca92bb21092dc8698a941db51cb546f06b53150"; break;
        case "TCO"  : nContractAddress = "0xf34405afd5d3a159600f2a847cc22b2e2b09ad8f"; break;
        case "CNUS" : nContractAddress = "0x63d863cffc7277a0e94f4397c62b5df9f691cbcd"; break;
        default:
            console.log('current Symbol not found: '+ nSymbol);
            return 'fail';
    }
    console.log('nContractAddress='+ nContractAddress);
    var nTxList, nSendIdx=0;
    var nToAddress, nValue; 
    var nTotalCnt =0, nResLog = 0;
    var nBeforeNum = nblockNum - 2;
    console.log('nBeforeNum = ' + nBeforeNum);
    console.log('nBlockNum  = ' + nblockNum);

    var nFilter = web3.eth.filter({"fromBlock":nBeforeNum, "toBlock":nblockNum, "address":nContractAddress});
    nFilter.get(function (error, Log) {
	if(error){
		console.log('ErrCase...FilterGet');
		return 'fail';
	}
        nResLog = JSON.stringify(Log, null, 2);
        //console.log('nResLength:'+ Log.length);
        nTotalCnt = Log.length;
        fsLog.apiLog.info('Erc20['+ nSymbol + '] FilterLength='+ nTotalCnt);
        nTxList = Log;
        for(var i=0; i<nTotalCnt; i++) {
            //console.log('Cnt['+i+'] TxHash: ' + nTxList[i].transactionHash);
            //console.log('Cnt['+i+'] Topics: ' + nTxList[i].topics[2]);
            //console.log('Cnt['+i+'] Data: ' + nTxList[i].data);
            //console.log('Cnt['+i+'] Coin: ' + web3.toDecimal(nTxList[i].data));
                
            var nTxHash = nTxList[i].transactionHash;
            var nTx = web3.eth.getTransactionFromBlock(nTxList[i].blockHash, nTxList[i].transactionIndex);

            // Input Data MethodID: Transfer Catch!
            var nInput = nTx.input.replace(/^0x/, ``);
            var nMethodId = nInput.slice(0,8);
            if(nMethodId) {
                if(nMethodId == 'a9059cbb') { // Transfer Case 
                    nToAddress = nInput.slice(32,72); 
                    //nWallet = '0x'+ nToAddress;
                    listAcc.forEach(function(nWallet) {
                        if(nWallet == '0x'+ nToAddress) {
                            fsLog.apiLog.info('------------------Response Data--/getDepositToken-');
                            fsLog.apiLog.info(' Contarct Tansfer To=ListAccount! Catch');
                            fsLog.apiLog.info('------------------Response Data--/getDepositToken-');
                            // value setting
                            if(nSymbol == 'gto'){
                                nValue = web3.toDecimal(nTxList[i].data)/100000;
                                fsLog.apiLog.info('gto contract case: beforeValue['+ web3.toDecimal(nTxList[i].data)+ ']');
                                fsLog.apiLog.info('gto contract case: afterValue['+ nValue +']');
                            }else {
                                nValue = web3.fromWei(web3.toDecimal(nTxList[i].data, 'ether'));
                            }

                            var txSendMsg = JSON.stringify({
                                nodeId: 1,
                                txId: nTxList[i].transactionHash,
                                fee: 0,
                                receiveAddress: nWallet,
                                volume: nValue,
                                confirmations: 1,
                                blockHash: nTxList[i].blockHash,
                                state: 'C'
                            })
                            sendTokenMsg(nSymbol, txSendMsg, nSendIdx, web3);
                            nSendIdx++;                                
                        }
                    }); // #end listAcc.forEach
                }
            }// #endif nMethodId
        }// #end for
    });


    return 'success';
}

module.exports = {
    getDepositToken: getDepositToken,
    getDepositEth: getDepositEth
};

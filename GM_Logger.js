var winston = require('winston');
var winstonDaily = require('winston-daily-rotate-file');
var moment = require('moment');
const MESSAGE = Symbol.for('message');

const logDir = 'logs';

const fs = require('fs');
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

function timeStampFormat() {
    return moment().format('YYYY-MM-DD HH:mm:ss.SSS ZZ');
};
console.log(timeStampFormat());

const jsonFormatter = (logEntry) => {
    const base = { timestamp: new Date() };
    const json = Object.assign(base, logEntry)
    logEntry[MESSAGE] = JSON.stringify(json);
    return logEntry;
}

var dpSysLog = winston.createLogger({
    transports: [
        new (winstonDaily)({
            name: 'info-file',
            dirname: logDir,
            filename: 'dpSysLog-%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            maxsize: '50m',
            maxFiles: '14d',
            level: 'debug',
            format: winston.format(jsonFormatter)()
        }),
        new (winston.transports.Console)({
            name: 'info-console',
            colorize: true,
            level: 'info',
            format: winston.format(jsonFormatter)()
        })
    ],
    exceptionHandlers: [
        new (winstonDaily)({
            name: 'exception-file',
            dirname: logDir,
            filename: 'dpSysLog-exception-%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            colorize: false,
            maxsize: '50m',
            maxFiles: '14d',
            level: 'error',
            format: winston.format(jsonFormatter)()
        }),
        new (winston.transports.Console)({
            name: 'exception-console',
            colorize: true,
            level: 'error',
            format: winston.format(jsonFormatter)()
        })
    ]
});

var dpEventLog = winston.createLogger({
    transports: [
        new (winstonDaily)({
            name: 'info-file',
            dirname: logDir,
            filename: 'dpEventLog-%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            maxsize: '50m',
            maxFiles: '14d',
            level: 'debug',
            format: winston.format(jsonFormatter)()
        }),
        new (winston.transports.Console)({
            name: 'info-console',
            colorize: true,
            level: 'info',
            format: winston.format(jsonFormatter)()
        })
    ],
    exceptionHandlers: [
        new (winstonDaily)({
            name: 'exception-file',
            dirname: logDir,
            filename: 'dpEventLog-exception-%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            colorize: false,
            maxsize: '50m',
            maxFiles: '14d',
            level: 'error',
            format: winston.format(jsonFormatter)()
        }),
        new (winston.transports.Console)({
            name: 'exception-console',
            colorize: true,
            level: 'error',
            format: winston.format(jsonFormatter)()
        })
    ]
});

var apiLog = winston.createLogger({
    transports: [
        new (winstonDaily)({
            name: 'info-file',
            dirname: logDir,
            filename: 'apiLog-%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            maxsize: '50m',
            maxFiles: '14d',
            level: 'debug',
            format: winston.format(jsonFormatter)()
        }),
        new (winston.transports.Console)({
            name: 'info-console',
            colorize: true,
            level: 'info',
            format: winston.format(jsonFormatter)()
        })
    ],
    exceptionHandlers: [
        new (winstonDaily)({
            name: 'exception-file',
            dirname: logDir,
            filename: 'apiLog-exception-%DATE%.log',
            datePattern: 'YYYY-MM-DD',
            colorize: false,
            maxsize: '50m',
            maxFiles: '14d',
            level: 'error',
            format: winston.format(jsonFormatter)()
        }),
        new (winston.transports.Console)({
            name: 'exception-console',
            colorize: true,
            level: 'error',
            format: winston.format(jsonFormatter)()
        })
    ]
});

module.exports = {
    dpSysLog:   dpSysLog,
    dpEventLog: dpEventLog,
    apiLog:      apiLog
}


//{ error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }
//logger.debug('디버그 로깅');
//logger.info('인포 로깅');
//logger.error('에러 로깅');

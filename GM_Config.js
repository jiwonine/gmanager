
function cfgRoad () {

    /* 1. Config Read */
    var cfg     = require('./config/GM_config.json');

    if(!cfg.RpcConn) {
        console.log('config/GM_config.json RpcConn invalid');
        console.log('RpcConn LocalSetting.. 127.0.0.1:16812');
        this.cfgRpcConn = '127.0.0.1:16812';
    }else {
        this.cfgRpcConn = cfg.RpcConn;
    }
    
    if(!cfg.SVCWallet) {
        console.log('config/GM_config.json SVCWallet URL is NULL');
        console.log('SVCWallet URL Setting...qa/local http://211.251.239.166:9701/');
        this.cfgSvcWallet = 'http://211.251.239.166:9701/';
    }else {
        this.cfgSvcWallet = cfg.SVCWallet;
    }
    
    if(cfg.Mode == 'test_mode') {
        this.cfgTest = '7e2B10937Bd4735215993B4B676353De97c11C02';
        this.cfgMode = 'test_mode';
        this.cfgPasswd = 'test.com';
    }else {
        this.cfgMode = 'real';
    }
    
    var dpTimer;
    if(!cfg.DpTimer) {
        console.log('config/dpTimer invalid');
        this.cfgDpTimer = 300000; // 5min
    }else {
        dpTimer = cfg.DpTimer * 60 * 1000;
        this.cfgDpTimer = dpTimer;
    }

    this.result = true;
    this.print = function() {
        console.log('--------------------GMConfig.json--------');
        console.log('|RpcConn   |'+ this.cfgRpcConn);
        console.log('|DBConn    |'+ this.cfgDBConn);
        console.log('|SVCWallet |'+ this.cfgSvcWallet);
        console.log('|Mode      |'+ this.cfgMode);
        console.log('|DpTimer   |'+ cfg.DpTimer + ' min = '+ this.cfgDpTimer+' sec');
        console.log('-----------------------------------------');
    };

    return this.result;
};

module.exports = {
    cfgRoad: cfgRoad
};


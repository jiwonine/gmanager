
var gManager    = require('./index.js');
var gm_Cfg      = gManager.gCfg;
var gm_Chalk    = gManager.mychalk;
var gm_Figlet   = gManager.myfiglet;
var gm_Api		  = gManager.gApi;

var		express	 = require('express');
var		http 		 = require('http');
var 	path		 = require('path');
var 	bodyParser  =  require('body-parser');
var 	static      = require('serve-static');

console.log(
    gm_Chalk.yellow(
        gm_Figlet.textSync('G Manager', { horizontalLayout: 'full'})
    )
);

var gCfgFunc = gm_Cfg.cfgRoad();
if(!gCfgFunc) {
    console.error('Geth Manager Config Read Fail', gm_Chalk.cyan('#'));
    return;
}
gm_Cfg.print();

var gMain = express();
gMain.set('port', process.env.PORT || 5080);
gMain.use('/', static(path.join(__dirname, '/')));
gMain.use(bodyParser.urlencoded({extended:true}));
gMain.use(bodyParser.json());

var gMainSv = http.createServer(gMain).listen(gMain.get('port'), function(){
	console.log(gm_Chalk.blueBright('G Manager Server is starting : ' + gMain.get('port') + '#####'));
});

// G Manager MainProcess
var DpEvent         = gManager.gDpEvent;
var gDpEvent        = new DpEvent();

//var MyDB            = gManager.gMongoDb;
//var gMongoDb        = new MyDB();

// Error Print
gMainSv.on('error', (err) => {
	console.error(gm_Chalk.red('## GManager Error : ' + err));
});


var gMApi = express();
gMApi.set('port', process.env.PORT || 5082);
gMApi.use('/', static(path.join(__dirname, '/')));
gMApi.use(bodyParser.urlencoded({extended:true}));
gMApi.use(bodyParser.json());

var gMApiSv = http.createServer(gMApi).listen(gMApi.get('port'), function(){
	console.log(gm_Chalk.cyan('G Manager Api Server is starting : ' + gMApi.get('port') + '#####'));
});

// Error Print
gMApiSv.on('error', (err) => {
		console.error(gm_Chalk.red('## GApi Error : ' + err));
});

gMApi.post('/getDepositToken', function (req, res) {
    console.log('/getDepositToken Start');
    /* input: symbol, blockNumber */
    var result = gm_Api.getDepositToken(req, res);
    console.log('/getDepositToken End result :' + result);
});

gMApi.post('/getDepositEth', function (req, res) {
    console.log('/getDepositEth Start');
    /* input: blockNumber */
    var result = gm_Api.getDepositEth(req, res);
    console.log('/getDepositEth End result :' + result);
});

var gCfgFunc = gm_Cfg.cfgRoad();
if(!gCfgFunc) {
    console.error('Geth Manager Config Read Fail', gm_Chalk.cyan('#'));
    return;
}
gm_Cfg.print();


var mShutdown = function() {
    console.log('Closed Geth Manager', gm_Chalk.red('###'));
    process.exit(0);
}

process.on('SIGINT', mShutdown);
process.on('SIGTERM', mShutdown);

module.exports = gDpEvent;

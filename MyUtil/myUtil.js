'use strict';

/**
 * 0x + Address = 0x'Address'
 */
exports.add0x = function (input) {
    if (typeof(input) !== 'string') {
      return input;
    }
    else if (input.length < 2 || input.slice(0,2) !== '0x') {
      return '0x' + input;
    }
    else {
      return input;
    }
};

/**
 * 0xAddress - 0x = 'Address'
 */
exports.strip0x = function (input) {
    if (typeof(input) !== 'string') {
      return input;
    }
    else if (input.length >= 2 && input.slice(0,2) === '0x') {
      return input.slice(2);
    }
    else {
      return input;
    }
};

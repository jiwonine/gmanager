module.exports = {
    fs:     require('fs'),
    env:    require('getenv'),
    mysync:  require('async'),
    mywait:  require('await'),
    mychai:   require('chai'),
    mychalk:  require('chalk'),
    myfiglet: require('figlet'),
    gDpEvent:   require('./GM_DpEvent.js'),
    gCfg:       require('./GM_Config.js'),
    gLog:       require('./GM_Logger.js'),
    gApi:	require('./GM_Api.js') 
}

'use strict';
// Require
var fs      =  require('fs');
var async   =  require('async');
var await   =  require('await');
var chai    =  require('chai');
var chalk   =  require('chalk');
var asset   =  chai.assert;
var fetch   =  require('node-fetch');

var Web3        = require('web3');
var _           = require('lodash');
var GMConfig    = require('./GM_Config.js');
var GMLog       = require('./GM_Logger.js');
var GMUtil      = require('./MyUtil/myUtil.js');

var GMWeb3;


// Timer Value
var CONNECTION_ATTEMPTS_TIMEOUT = 5000;
var CONTRACT_EVENT_INTERVAL     = 180000; //300000; // 5min * 60 * 1000
var PEER_CONN_INTERVAL          = 40000; // 40second
var WALLET_CHECK_INTERVAL       = 5000; // test
var MAX_CONNECTION_ATTEMPTS     = 50;


function GMDpEvent () {

	this._gpweb3            = false;
	this._gpconn_attempts   = 0;
    
    this.currentBlock      = 0;
    this.lastBlock         = 0;
    this.allAccounts       = 0;

	/*-- DepositEvent Timer --*/
	this.ctEventInterval    = false;
    this._ctEventCheck      = 0;
    
    /*-- AddWallet Check Timer --*/
    this.ctWalletInterval   = false;
    this._ctWalletCheck     = 0;

    /*-- Web3Connection Timer --*/
    this.web3ConnInterval   = false;
    this._Web3ConnCheck     = 0;

    this.cfgInfo = {
        rpcUrl:     (GMConfig.cfgRpcConn || "http://0.0.0.0:16812"),
        dpTimer:    (GMConfig.cfgDpTimer || CONTRACT_EVENT_INTERVAL),
        svcUrl:     (GMConfig.cfgSvcWallet || "http://211.251.239.166:9701/"),
        mode:       (GMConfig.cfgMode)
    }

    this.cfgInfoPrint = function() {
        console.log('--------------------GMDpEvent cfgInfo--------');
        console.log('|RpcConn   |'+ this.cfgInfo.rpcUrl);
        console.log('|Mode      |'+ this.cfgInfo.mode);
        console.log('|DpTimer   |'+ this.cfgInfo.dpTimer);
        console.log('|SvcWallet |'+ this.cfgInfo.svcUrl);
        console.log('---------------------------------------------');
    };

    this.cfgContract = {
        ecInfo_GNT:     'a74476443119a942de498590fe1f2454d7d4ac0d',
        ecInfo_GTO:     'c5bbae50781be1669306b9e001eff57a2957b09d',
        ecInfo_NPXS:    'a15c7ebe1f07caf6bff097d8a589fb8ac49ae5b3',
        ecInfo_OMG:     'd26114cd6ee289accf82350c8d8487fedb8a0c07',
        ecInfo_SNT:     '744d70fdbe2ba4cf95131626614a1763df805b9e',
        ecInfo_OBSR:    'cb8a874ad773b10e6cf3b6b6de9dcc3b2ff68a9e',
        ecInfo_PAX:     '67fe8dbf5ed0aa9d7a127e5e061d715a1eff3712',
        ecInfo_AMO:     '38c87aa89b2b8cd9b95b736e1fa7b612ea972169',
        ecInfo_ABLX:    '865bfd8232778f00cae81315bf75ef1fe6e30cdd', 
        ecInfo_ABLD:    '5cfaf9ad2cb84ce2bc40be1beb735bd12a075e99',
        ecInfo_TCOR:    '4ca92bb21092dc8698a941db51cb546f06b53150', 
        ecInfo_TCO:     'f34405afd5d3a159600f2a847cc22b2e2b09ad8f',
        ecInfo_CNUS:    '63d863cffc7277a0e94f4397c62b5df9f691cbcd', 
        ttInfo_WONI:    'e249be1b31ecd0c4fcef46d6d07516e8dfef7ba2',
        ttInfo_UDAP:    '68f2cbda42edb8f2a4da853b34e772a5f8e98ce0'
    };

    this.cfgInfoPrint();
    this.startWeb3Conn();
    this.loadWalletData();
    this.getDepositEvent(true);
    this.setDpEvent();
	return this;
}

GMDpEvent.prototype.loadWalletData = function() {
    var self = this;
    if(!this._gpweb3) {
        return false;
    }else {
        var nInFileName = './wData.dat';
        // loadWawlletData
        var nData = GMWeb3.eth.accounts;
        //var nFullData = '[' + nData + ']';
        var nFullData = nData;
        //console.log('nData='+ nData);
        //console.log('nFullData='+ nFullData);
        fs.writeFile('./wData.dat', nFullData, 'utf8', function (err) {
            if(err) throw err;
            console.log('wData.dat write Succ');
        });         
    }
}

GMDpEvent.prototype.startWeb3Conn = function() {
    GMLog.dpSysLog.info('[startWeb3Conn] START');
    GMLog.dpSysLog.info('[startWeb3Conn] rpcConnInfo: ' +  this.cfgInfo.rpcUrl);
	GMWeb3 = new Web3();
	GMWeb3.setProvider(new GMWeb3.providers.HttpProvider(this.cfgInfo.rpcUrl));
	this.checkWeb3Conn();
	GMLog.dpSysLog.info('[startWeb3Conn] END');
}

GMDpEvent.prototype.checkWeb3Conn = function() {
	var self = this;
	if (!this._gpweb3) {
		if(GMWeb3.isConnected()) {
            console.log('[checkWeb3Conn] WEB3 connection Established');
			this._gpweb3 = true;
            this.setWebConn();
			return true;
		}else {
			if(this._gpconn_attempts < MAX_CONNECTION_ATTEMPTS) {
                GMLog.dpSysLog.error('[checkWeb3Conn] WEB3 connection attempt' + this._gpconn_attempts + ' failed');
                GMLog.dpSysLog.error('[checkWeb3Conn] Trying again in ' + this._gpconn_attempts);
				setTimeout(function () {
					self.checkWeb3Conn();
				}, CONNECTION_ATTEMPTS_TIMEOUT);
			}else {
                GMLog.dpSysLog.error('[checkWeb3Conn] WEB3 connection failed, times. Aborting...');
			} // #endif ConnectionAttempts
		} // #endif GMWeb.isConnected
	}
}

GMDpEvent.prototype.reConnWeb3 = function() {
	this._gpweb3 = false;
	this._gpconn_attempts = 0;

    if(this.web3ConnInterval) {
        clearInterval(this.web3ConnInterval);
    }
	try{
		GMWeb3.reset(true);
	} catch (err) {
        GMLog.dpSysLog.error('[reConnWeb3] reset Error:', err);
	}
	this.checkWeb3Conn();
}

GMDpEvent.prototype.getSymbolAddr = function(symbol) {
    var nContractAddr;
    if(this.cfgInfo.mode == 'test_mode') {
        GMLog.dpSysLog.debug('[getSymbolAddr] test_mode ContractAddress Setting');
        switch(symbol) {
            case "WONI" : nContractAddr = GMUtil.add0x(this.cfgContract.ttInfo_WONI); break;
            case "UDAP" : nContractAddr = GMUtil.add0x(this.cfgContract.ttInfo_UDAP); break;
            default: {
                GMLog.dpSysLog.error('[getSymbolAddr] test_mode current Symbol not found: '+ symbol);
                return false;
            }
            break;
        }
    }else {
        GMLog.dpSysLog.debug('[getSymbolAddr] real ContractAddress Setting');
        switch(symbol) {
            case "GNT"  : nContractAddr = GMUtil.add0x(this.cfgContract.ecInfo_GNT); break;
            case "GTO"  : nContractAddr = GMUtil.add0x(this.cfgContract.ecInfo_GTO); break;
            case "NPXS" : nContractAddr = GMUtil.add0x(this.cfgContract.ecInfo_NPXS); break;
            case "OMG"  : nContractAddr = GMUtil.add0x(this.cfgContract.ecInfo_OMG); break;
            case "SNT"  : nContractAddr = GMUtil.add0x(this.cfgContract.ecInfo_SNT); break;
            case "OBSR" : nContractAddr = GMUtil.add0x(this.cfgContract.ecInfo_OBSR); break;
            case "PAX"  : nContractAddr = GMUtil.add0x(this.cfgContract.ecInfo_PAX); break;
            case "AMO"  : nContractAddr = GMUtil.add0x(this.cfgContract.ecInfo_AMO); break;
            case "ABLD" : nContractAddr = GMUtil.add0x(this.cfgContract.ecInfo_ABLD); break;
            case "ABLX" : nContractAddr = GMUtil.add0x(this.cfgContract.ecInfo_ABLX); break;
            case "TCO"  : nContractAddr = GMUtil.add0x(this.cfgContract.ecInfo_TCO); break;
            case "TCO-R": nContractAddr = GMUtil.add0x(this.cfgContract.ecInfo_TCOR); break;
            case "CNUS" : nContractAddr = GMUtil.add0x(this.cfgContract.ecInfo_CNUS); break;
            default: {
                GMLog.dpSysLog.error('[getSymbolAddr] real current Symbol not found: '+ symbol);
                return false;
            }
            break;
        }
    }
    return nContractAddr;
}

GMDpEvent.prototype.sendTxMsg = function(symbol, txMsg, idx) {
    var nDepositUrl = this.cfgInfo.svcUrl.concat(symbol, '/wallet/deposit/add');
    
    const nResponse = fetch(nDepositUrl, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: txMsg
    }).then(function(res) {
        if (res.status != 200) {
            GMLog.dpEventLog.info('SendTxMsg.ResCode('+ res.status + '), ResMsg='+ res.statusText);
            GMLog.dpEventLog.info('SendTxMsg.TxBody=' + txMsg);
        }else {
            GMLog.dpEventLog.info('SendTxMsg.Erc20['+ symbol +'].['+ idx +'] 200OK');
            GMLog.dpEventLog.info('SendTxMsg.TxBody=' + txMsg);
        }
    }).catch(err=> {
        GMLog.dpEventLog.error('[sendTxMsg] ErrMsg:'+ err);
        throw err;
    });
}

GMDpEvent.prototype.chkDepositContract = function(symbol, fromBlock, lastBlock, mycallBack) {
    GMLog.dpSysLog.info('[chkDepositContract] START');
    var self    = this;

    async.waterfall([
        function(callback) {
            // 1. getContractAddr (input:symbol)
            var nContractAddr = self.getSymbolAddr(symbol);
            if(!nContractAddr){
                callback('getSymbolAddr ['+ symbol +'] not found! Fail');
                return false;
            }
            callback(false, nContractAddr, fromBlock, lastBlock);
        },
        function(ContractAddr, from, to, callback) {
            // 2. GetData, WEB3.eth.filter 
            var nFilter;
            try {
                var nFilter = GMWeb3.eth.filter({"fromBlock":from, "toBlock":to, "address":ContractAddr});
               if(!nFilter) {
                callback('GMWeb3.eth.filter() null! Fail');
                return false;
                 }
                var nResLog;
                var nTotalCnt=0, nDataValue=0, nValue=0, nSendIdx=0;
                var nFirstTxIdx = 0, nLastTxIdx = 0;
                var nTxList, nTxHash, nTx, nToAddress;
                nFilter.get(function (error, Log) {
                    if(error) {
                        callback('web3.eth.filter.get Fail('+ error +')');
                        return false;
                    }
                    //JSON.stringify(Log);  //console.log('LogValue='+ nResultData);
                    //nResLog = JSON.stringify(Log, null, 2);
                    //console.log(nResLog);

                    // 2-(1). Catch Count
                    nTotalCnt   = Log.length;
                    nTxList     = Log;
                    GMLog.dpEventLog.info('[chkDepositContract] Symbol='+ symbol + ', FilterLength='+ nTotalCnt);
                    if(nTotalCnt > 1){ 
                        nFirstTxIdx = nTxList[0].transactionIndex; 
                        nLastTxIdx  = nTxList[nTotalCnt-1].transactionIndex;
                        GMLog.dpSysLog.info('[chkDepositContract] nFirstTxIdx='+ nFirstTxIdx + ' , nLastTxIdx=' + nLastTxIdx);

                        // 2-(2). Exclude MultiTransfer (AirDrop)
                        if(nFirstTxIdx == nLastTxIdx){
                            GMLog.dpSysLog.info('[chkDepositContract] AirDrop/MultiTansfer Case');
                            callback('AirDrop/MultiTransfer Case');
                            return false;
                        }
                    }
        
                    // 2-(3). Transfer Method Catch (For)
                    for(var i=0; i<nTotalCnt; i++) {
                        nTxHash = nTxList[i].transactionHash;
                        var nTx = GMWeb3.eth.getTransactionFromBlock(nTxList[i].blockHash, nTxList[i].transactionIndex);
                        if(!nTx) { continue; }
                        // Tx Data Parsing
                        var nInput = nTx.input.replace(/^0x/, ``);
                        var nMethodId = nInput.slice(0,8);
                        var nFullToAddress = undefined;
                        nValue=0;
                        if(nMethodId == 'a9059cbb'){
                                nToAddress = undefined;
                                nToAddress = nInput.slice(32,72);
                                nFullToAddress = GMUtil.add0x(nToAddress);
                                // value setting
                                if(symbol == 'GTO'){
                                    //nValue = GMWeb3.toDecimal(nTxList[i].data)/100000;
                                    nDataValue = GMWeb3.toDecimal(nTxList[i].data, function(err, nValue) {
                                        if (err) {
                                            GMLog.dpSysLog.error('[chkDepositContract] toDecimal Error:'+ err);
                                            callback('web3.toDecimal('+ err +')');
                                            return false;
                                        }
                                        //nValue = nDataValue/100000;
                                    });
                                    nValue = nDataValue/100000;
                                    GMLog.dpSysLog.info('[chkDepositContract] GTO Contract Case: beforeValue['+ nDataValue +']');
                                    GMLog.dpSysLog.info('[chkDepositContract] GTO Contract Case: afterValue['+ nValue +']');
                                }else {
                                    nValue = GMWeb3.fromWei(GMWeb3.toDecimal(nTxList[i].data, 'ether'));
                                }// #endif symbol

                				var txSendMsg = JSON.stringify({
                				nodeId: 1,
                				txId: nTxList[i].transactionHash,
                				fee: 0,
                				receiveAddress: nFullToAddress,
                				volume: nValue,
                				confirmations: 1,
                				blockHash: nTxList[i].blockHash,
                				state: 'C'
                				})
                				self.sendTxMsg(symbol, txSendMsg, nSendIdx);
                				nSendIdx++;
                        }// #endif nMethodId = Transfer
                    }// #end for
                    GMLog.dpSysLog.info('[chkDepositContract] END!!! symbol:' + symbol + 'sendTxCnt:' + nSendIdx);
                    callback(null);
                });
            }catch (err) {
                callback(err);
            }
        }    
    ], function(err, callback) {
        if (err) {
            mycallBack(new Error (err));
            return false;
        }
        return true;
    });
}

GMDpEvent.prototype.getDepositEvent = function (forced) {
    var self            = this;
    var nowTime         = _.now();
    var lastCheckAgo    = nowTime - this._ctEventCheck;
    this._ctEventCheck  = nowTime;

    console.log('forced:' + forced);
    // lastCheckAgo >= CONTRACT_EVENT_INTERVAL
    if (this._gpweb3 && (lastCheckAgo >= CONTRACT_EVENT_INTERVAL || forced == true)) {
        GMLog.dpSysLog.info('[getDepositEvent] getDepositEvent Case!!!');
        var nCurBlock = 0, nLastBlock = 0;
	    nCurBlock = GMWeb3.eth.getBlockNumber(function (error, result){ 
		    if(error) {
			    GMLog.dpSysLog.error('[getDepositEvent] WEB3Error getBlockNumber:' + error);
			    return;
		    }
		    GMLog.dpSysLog.debug('[getDepositEvent] eth.getBlockNumber()= '+ result);
            self.lastBlock = result;
            GMLog.dpSysLog.debug('[getDepositEvent] setting self.lastBlock()= '+ self.lastBlock);
	    });

        // Setting nLastBlock, nCurBlock
        if(self.currentBlock == 0){
            nCurBlock   = 0;
            nLastBlock  = self.lastBlock;
        }else {
            nCurBlock   = self.currentBlock;
            nLastBlock  = self.lastBlock; 
        }
        
        GMLog.dpSysLog.info('[getDepositEvent] nCurBlock:'+ nCurBlock + ', nLastBlock:'+ nLastBlock);

        // Equal Case, PASS..
        
        if(self.currentBlock == self.lastBlock) {
            console.log('[getDepositEvent] Equal Count!, chkDepositContract PASS!!');
            return;
        }
 
        GMLog.dpSysLog.debug('[getDepositEvent] nCurBlock:'+ nCurBlock + ', nLastBlock:'+ nLastBlock);
        console.log('[getDepositEvent] this._currentBlock Before:'+ self.currentBlock);
        self.currentBlock = self.lastBlock;
        console.log('[getDepositEvent] this._currentBlock After:'+ self.currentBlock);
        async.parallel({
                /*symbol_WONI: function (callback) {
                   // self.chkDepositContract('WONI', nCurBlock, nLastBlock); // 3699477, 3781600
                   self.chkDepositContract('WONI', 3699477, 3781600, callback);
                },
                symbol_UDAP: function (callback) {
                    //self.chkDepositContract('udap', nCurBlock, nLastBlock); //3923415, 3923418);
                    self.chkDepositContract('UDAP', 3923415, 3923418, callback);
                },*/
                sb_GNT: function (callback) {
                    self.chkDepositContract('GNT', nCurBlock, nLastBlock, callback);
                },
                sb_GTO: function (callback) {
                    self.chkDepositContract('GTO', nCurBlock, nLastBlock, callback);
                }, 
                sb_NPXS: function (callback) {
                    self.chkDepositContract('NPXS', nCurBlock, nLastBlock, callback);
                },
                sb_OMG: function (callback) {
                    self.chkDepositContract('OMG', nCurBlock, nLastBlock, callback);
                },
                sb_SNT: function (callback) {
                    self.chkDepositContract('SNT', nCurBlock, nLastBlock, callback);
                },
                sb_OBSR: function (callback) {
                    self.chkDepositContract('OBSR', nCurBlock, nLastBlock, callback);
                },
                sb_PAX: function (callback) {
                    self.chkDepositContract('PAX', nCurBlock, nLastBlock, callback);
                },
                sb_AMO: function (callback) {
                    self.chkDepositContract('AMO', nCurBlock, nLastBlock, callback);
                },
                sb_ABLX: function (callback) {
                    self.chkDepositContract('ABLX', nCurBlock, nLastBlock, callback);
                },
                sb_ABLD: function (callback) {
                    self.chkDepositContract('ABLD', nCurBlock, nLastBlock, callback);
                },
                sb_TCO: function (callback) {
                    self.chkDepositContract('TCO', nCurBlock, nLastBlock, callback);
                },
                sb_TCOR: function (callback) {
                    self.chkDepositContract('TCO-R', nCurBlock, nLastBlock, callback);
                },
                sb_CNUS: function (callback) {
                    self.chkDepositContract('CNUS', nCurBlock, nLastBlock, callback);
                }
            },
            function (err, results) {
                if (err) {
                    GMLog.dpSysLog.error('[getDepositEvent]|ERR| ' + err);
                    return false;
                }
                return true;
            });
    }
    GMLog.dpSysLog.info('[getDepositEvent] END');
}

GMDpEvent.prototype.getAddWalletCheck = function() {
    GMLog.dpSysLog.info('[getAddWalletCheck] START');
    var self = this;
    if(this._gpweb3){
        var nTotalAcc = GMWeb3.eth.accounts.length;
        console.log('nTotalAcc=' + nTotalAcc);
        self.allAccounts = nTotalAcc;
        console.log('allAccounts Setting=' + self.allAccounts);
    }else {
        GMLog.dpSysLog.error('[getAddWalletCheck] web3 connection check');
    }
    GMLog.dpSysLog.info('[getAddWalletCheck] END');
}

GMDpEvent.prototype.setWebConn = function () {
	var self = this;
    this.web3ConnInterval = setInterval( function() {
		self.reConnWeb3();
    }, CONNECTION_ATTEMPTS_TIMEOUT);

}

GMDpEvent.prototype.setDpEvent = function () {
    var self = this;
    /* Contract Event Catch Interval */
    console.log('setDpEvent Timer SETTING');
	this.ctEventInterval = setInterval( function() {
		if(self._gpweb3){
            self.getDepositEvent();
		}else {
			GMLog.dpSysLog.error('==> ctEventInterval web3 Connection Fail');
		}
    }, CONTRACT_EVENT_INTERVAL);
}

GMDpEvent.prototype.init = function () {
    this.setWebConn();
    this.setDpEvent();
}

module.exports = GMDpEvent;
